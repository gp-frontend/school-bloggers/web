let paths = {
  'pug': 'src/website/**/*.pug',
  'styles': 'src/website/**/*.pcss',
  'scripts': 'src/scripts/**/*.js',
  'images': 'src/images/content/**/*',
  'uploads': 'src/images/content/**/*',
  'icons': 'src/icons/*.svg',
  'iconsForBase64': 'src/icons/base/*.svg',
  'json': 'src/website/utils/data/json/*.json',
  'staticFiles': 'src/static/**/*'
};

module.exports = () => {

  $.gulp.task('watch', () => {

    $.gulp.watch(paths.pug, $.gulp.series('pug'));
    $.gulp.watch(paths.styles, $.gulp.series('styles:dev'));
    $.gulp.watch(paths.scripts, $.gulp.series('scripts:dev'));
    $.gulp.watch(paths.icons, $.gulp.series('svg:sprite'));
    $.gulp.watch(paths.iconsForBase64, $.gulp.series('svg:base'));
    $.gulp.watch(paths.json, $.gulp.series('json:merge'));
    $.gulp.watch(paths.staticFiles, $.gulp.series('copy'));
    $.gulp.watch(paths.images, $.gulp.series(
      $.gulp.parallel(
        'images:copy',
        'images:gif',
        'images:svg'
      ))
    );
    $.gulp.watch(paths.uploads, $.gulp.series(
      $.gulp.parallel(
        'uploads:copy',
        'images:gif',
        'images:svg'
      ))
    );
  });
};
