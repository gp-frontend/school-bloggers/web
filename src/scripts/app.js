(function ($) {

  /*----------------------------------------
  FOR SVG SPRITE
----------------------------------------*/
  svg4everybody({});

  /*----------------------------------------
   TRANSITION SCROLL
 ----------------------------------------*/
  $('.js-link-scroll').on('click', function () {

    var anchor = $(this),
        headerHeight = $(".header").height();

    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top - headerHeight
    }, 1000)
  });

  /*----------------------------------------
   SELECTIZE INIT
  ----------------------------------------*/

  $('.sumo-select').SumoSelect();

  //WIDTH SCROLL BAR
  var scrollBar = document.createElement('div');

  scrollBar.style.overflowY = 'scroll';
  scrollBar.style.width = '50px';
  scrollBar.style.height = '50px';

  // при display:none размеры нельзя узнать
  // нужно, чтобы элемент был видим,
  // visibility:hidden - можно, т.к. сохраняет геометрию
  scrollBar.style.visibility = 'hidden';

  document.body.appendChild(scrollBar);
  var scrollWidth = scrollBar.offsetWidth - scrollBar.clientWidth;
  document.body.removeChild(scrollBar);

  /*----------------------------------------
  POPUPS / MODALS
  ----------------------------------------*/
  (function () {

    var ESC_KEYCODE = 27;

    var popup = $(".popup"),
      popupOpen = $("[data-popup='open']"),
      popupClose = $("[data-popup='close']"),
      popupOverlay = $(".popup__overlay");

    /* POPUP FUNCTIONS */
    $.fn.gpPopup = function( method ) {

      if ( methods[method] ) {
        return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
      } else if ( typeof method === 'object' || ! method ) {
        return methods.show.apply( this, arguments );
      } else {
        $.error( 'Метод с именем ' +  method + ' не существует' );
      }

    };

    // alert( scrollWidth );

    /* POPUP METHODS */
    var methods = {
      //show popup
      show : function( options ) {
        return this.each(function(){

          popup.removeClass('popup--open');
          $(this).addClass('popup--open');
          $('body').addClass('page--locked');

          if(scrollWidth > 0){
            $('body').css('padding-right', scrollWidth);
            $('header').css('padding-right', scrollWidth);
          }
        });
      },
      //hide all popups
      hide : function( ) {

        return this.each(function(){

          if (popup.hasClass('popup--open')) {
            popup.removeClass('popup--open');
            $('body').removeClass('page--locked').css('padding-right', '');
            $('header').css('padding-right', '');
          }

        })

      }
    };

    /* OPEN */
    function openPopup(e){
      e.preventDefault();
      var id = $(this).attr('data-popup-id');
      $(id).gpPopup();
    }

    /* CLOSE */
    function closePopup(e){
      e.preventDefault();
      popup.gpPopup('hide');
    }

    /* ADD CLICK FUNCTION */
    popupOpen.click(openPopup);
    popupClose.click(closePopup);
    popupOverlay.click(closePopup);

    /* PRESS ESC BUTTON */
    $(document).keydown(function(evt) {
      if( evt.keyCode === ESC_KEYCODE ) {
        popup.gpPopup('hide');
        return false;
      }
    });
  })();

  /*----------------------------------------
   MENU
  ----------------------------------------*/
  var menu = $("#menu"),
      buttonOpenMenu = $(".js-button-open-menu"),
      buttonCloseMenu = $(".js-button-close-menu");

  $('<div>', { class: 'menu__overlay js-menu-overlay'}).appendTo('.header');

  function openMenu() {
    menu.addClass("menu--open");
    $(".menu__overlay").addClass("menu__overlay--visible");
    $("body").addClass("page--locked");

    if(scrollWidth > 0){
      $('body').css('padding-right', scrollWidth);
    }
  }

  function closeMenu() {
    menu.removeClass("menu--open");
    $(".menu__overlay").removeClass("menu__overlay--visible");
    $("body").removeClass("page--locked").css("padding-right", "");
  }

  buttonOpenMenu.on("click", openMenu);
  buttonCloseMenu.on("click", closeMenu);
  $(".menu__link").on("click", closeMenu);
  $(".header__button-call").on("click", closeMenu);
  $(".js-menu-overlay").on("click", closeMenu);


  /*----------------------------------------
   INPUT MASK
  ----------------------------------------*/
  $(".js-mask-number").mask("0#");
  $(".js-mask-phone").mask("+7 (000) 000-00-00");
  $(".js-mask-time").mask("00 : 00");
  $(".js-mask-date").mask("00.00.0000");

  /*----------------------------------------
   CAROUSEL
  ----------------------------------------*/
  $(document).ready(function() {

    $('.js-carousel-news').slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        },
      ]
    });

    $('.js-carousel-groups').slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
          }
        },
      ]
    });

    $('.js-review-carousel').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 1620,
          settings: {
            arrows: false,
            dots: true
          }
        }
      ]
    });

    $('.js-recommendations').slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            slidesToShow: 2,
            dots: true,
            arrows: false,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            dots: true,
            arrows: false,
          }
        },
      ]
    });
  });

  // slider
  var $box = $(".js-carousel-box");

  settings_slider = {
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 979,
        settings: {
          slidesToShow: 2
        }
      },
    ]
  };

  // slick on mobile
  function slick_on_mobile(slider, settings){

    $(window).on('load resize', function() {

      if ($(window).width() > 1199 - scrollWidth) {

        if (slider.hasClass('slick-initialized')) {
          slider.slick('unslick');
        }

        return
      }
      if (!slider.hasClass('slick-initialized')) {
        return slider.slick(settings);
      }
    });
  }

  slick_on_mobile( $box, settings_slider);

  /*----------------------------------------
   VALIDATE FORMS
  ----------------------------------------*/

  $('#form-order').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-vacancy').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-review').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-popup-review').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  /*----------------------------------------
    HEADER PANEL
  ----------------------------------------*/

  var headerPanel = $('.js-location-panel'),
    buttonOpenHeaderPanel = $('.js-location'),
    buttonClosenHeaderPanel = $('.js-panel-close');

  $('<div>', { class: 'header-panel__overlay js-panel-overlay'}).appendTo('.header');

  function openHeaderPanel (e) {
    e.preventDefault();

    buttonOpenHeaderPanel.toggleClass('header__location--active');
    headerPanel.toggleClass('location-panel--open');
    $('.js-panel-overlay').toggleClass('header-panel__overlay--visible');
    $('body').toggleClass('page--locked');
  }

  function closeHeaderPanel (e) {
    e.preventDefault();

    buttonOpenHeaderPanel.removeClass('header__location--active');
    headerPanel.removeClass('location-panel--open');
    $('.js-panel-overlay').removeClass('header-panel__overlay--visible');
    $('body').removeClass('page--locked');
  }

  buttonOpenHeaderPanel.click(openHeaderPanel);
  buttonClosenHeaderPanel.click(openHeaderPanel);
  $('body .js-panel-overlay').click(closeHeaderPanel);
})(jQuery);
