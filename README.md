# Project: School bloggers 

## How to build project

**Install**

```bash
npm i
```

**How to run dev build**

```bash
gulp 
```

**How to compile production build**

```bash
gulp prod
```

## Source files

- **src** - main folder for all source files
- **website** - *.pug & *.pcss files for compile HTML and CSS
- **scripts** - all javascripts files  
- **icons** - svg images for svg symbol sprites
- **images** - for optimise images
- **static** - files not for compilation or changes
- static/**fonts** - all fonts for project
- static/**favicons** - folder for generated favicons from https://realfavicongenerator.net

## Usage JS libs

- Jquery
- Jquery mask
- Jquery validate
- Pace
- svg4everybody
- slick.js
- aos.js
